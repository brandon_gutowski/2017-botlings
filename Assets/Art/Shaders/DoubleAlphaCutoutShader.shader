﻿Shader "Custom/DoubleAlphaCutoutShader" {
	Properties { 
	_Color ("Color Tint", Color) = (1,1,1,1) 
	_MainTex ("Base (RGB) Trans (A)", 2D) = "white" {} 
	_CutTex ("Cutoff (A)", 2D) = "white" {} 
	_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5 }
	//_BumpMap ("Normal Texture", 2D) = "bump" {} 
	//_BumpDepth ("Bump Depth", Range(-2.0,2.0)) = 1

	SubShader { 
		Pass{
			Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"} 
			
				LOD 200

				CGPROGRAM 
				#pragma vertex vert
				#pragma fragment frag
				
				sampler2D _MainTex;
				float4 _MainTex_ST; 
				sampler2D _CutTex; 
				fixed4 _Color; 
				fixed _Cutoff;

				struct vertexInput { 
					float4 vertex : POSITION;
					float3 normal : NORMAL;
					float4 texcoord : TEXCOORD0;
					float4 texcoord1 : TEXCOORD1;			
				};
				
				struct vertexOutput {
					float4 pos : SV_POSITION;
					float4 tex : TEXCOORD0;
					float4 tex1 : TEXCOORD1;
					
				};
				
				vertexOutput vert(vertexInput v){
					
					vertexOutput output;
				
					output.pos = mul(UNITY_MATRIX_MVP, v.vertex);
					
					output.tex = v.texcoord;
					output.tex1 = v.texcoord1;
					
					return output;
				
				}
				
				float4 frag(vertexOutput input) : COLOR {
				
					float4 color = tex2D(_MainTex, float2(input.tex.xy));
					
					float4 tex = tex2D(_MainTex, input.tex.xy * _MainTex_ST.xy + _MainTex_ST.zw);
					float4 tex1 = tex2D(_CutTex, input.tex1.xyw);
					
					
					clip(_Cutoff - tex.a);
					tex.a = tex1.a;
									
					return float4(tex.xyz * _Color.xyz, tex.a);
				
				}		 
			
			ENDCG 
		}
	}

	Fallback "Diffuse" 
}