﻿using UnityEngine;
using System.Collections;

public class ContinueMusicPlaying : MonoBehaviour {

	void Awake() {

		//Find if there is a current music source in the scene
		GameObject currentMusic = GameObject.FindGameObjectWithTag("MusicSource");

		if (currentMusic != this.gameObject) {
			Destroy(this.gameObject);
			return;
		} else {
			currentMusic = this.gameObject;
		}

		DontDestroyOnLoad(this.gameObject);
	}
		
}
