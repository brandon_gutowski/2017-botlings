using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class BotSplosion : MonoBehaviour {

	public float mBotMass;
	public float mBotDrag;
	public float mMaxForce;
	public float mMinForce;
	public float mExplosionRadius;
	
	private List<Transform> mBotParts;
	
	private Transform mRobot;
	private Dictionary<string, Vector3> mSplodeDict = new Dictionary<string, Vector3>();
	
	private float mCountTime = 2f;
	
	private Vector3 mParentTransform;

	void Awake()
	{
		mRobot = this.transform;
	}

	void Start () 
	{
		SetExplodeParts();

	}

	void Update () 
	{
		mCountTime -= Time.deltaTime;
		if(mCountTime >= 0)
		{
			ExplodeParts ();
		}

	}

	public void SetExplodeParts()
	{
		mBotParts = mRobot.transform.GetComponentsInChildren<Transform>().ToList();
		mBotParts.ForEach (a => 
       	{
			SetExplodePart(a);
		});
	}
	
	private void SetExplodePart(Transform botPart)
	{
		if(botPart.gameObject.GetComponent<Rigidbody>() == null)
		{
			botPart.gameObject.AddComponent(typeof(Rigidbody));
		}

		Rigidbody rBody = botPart.GetComponent<Rigidbody>();

		rBody.mass = mBotMass;
		rBody.drag = mBotDrag;
		rBody.angularDrag = .1f;
		
		rBody.AddTorque 
		(
			Random.Range (mMaxForce, mMinForce) * 50,
			Random.Range (mMaxForce, mMinForce) * 50,
			Random.Range (mMaxForce, mMinForce * 50
      	));

		mSplodeDict.Add(botPart.name, new Vector3 (Random.Range (mMaxForce, mMinForce), Mathf.Abs(Random.Range (mMaxForce, mMinForce)) * 2, Random.Range (mMaxForce, mMinForce)) + Random.insideUnitSphere * mExplosionRadius);
	}

	public void ExplodeParts()
	{
		Rigidbody rBody;

		mBotParts.ForEach (a => 
		{
			rBody = a.GetComponent<Rigidbody>();
			rBody.AddForce(mSplodeDict[a.name] * Time.deltaTime * Mathf.Clamp((mCountTime/5f), 0, 5), ForceMode.Impulse);
		});
	}
}
