﻿using UnityEngine;
using System.Collections;

public class CannonArrow : MonoBehaviour {

	public float mTravelTimeEndPoint = 1f;
	public int mNumberOfDivisions = 1;
	public Transform mHomePoint;
	
	private LineRenderer mLineRenderer;

	private Vector3 mLaunchVector = new Vector3();
	private float mLaunchVelocity;
	private Cannon mCannonScript;

	void Awake(){

		mLineRenderer = this.GetComponent<LineRenderer> ();

		if (mLineRenderer == null) {
			Debug.LogWarning("No Line Renderer attached to CannonArow GameObject");
		}

		AssignLaunchVelocity ();

	}

	void Update(){
		if (CheckIfVisible ()) {
			UpdateLaunchVector ();
			mLineRenderer.enabled = true;
			UpdateRenderer();


		} else {
			mLineRenderer.enabled = false;
		}
	}

	void UpdateLaunchVector(){
		mLaunchVector = (this.transform.position - mHomePoint.position).normalized * mLaunchVelocity;
	}

	void UpdateRenderer(){

		mLineRenderer.SetVertexCount (mNumberOfDivisions);

		for (int i = 0; i < mNumberOfDivisions; i++) {
			mLineRenderer.SetPosition(i, DeterminePositionBasedOnTime(i * mTravelTimeEndPoint/mNumberOfDivisions));
		}

	}

	Vector3 DeterminePositionBasedOnTime(float time){
		Vector3 startPosition = mHomePoint.transform.position;
		startPosition.y = 0;

		startPosition.x = startPosition.x + mLaunchVector.x * time;
		startPosition.y = startPosition.y + mLaunchVector.y * time + 0.5f * Physics.gravity.y * time * time;		

		return startPosition;
	}

	bool CheckIfVisible(){

		if (mCannonScript.mStoredRobots > 0) {
			return true;
		} else {
			return false;
		}		                                   
	}

	void AssignLaunchVelocity(){

		Transform tempTransform = transform;
		bool foundCannonScript = false;

		while (!foundCannonScript) {

			tempTransform = tempTransform.parent;

			if(tempTransform.GetComponent<Cannon>() != null){
				mCannonScript = tempTransform.GetComponent<Cannon>();
				mLaunchVelocity = mCannonScript.mLaunchVelocity;
				foundCannonScript = true;
			}

			if(tempTransform.parent == null){
				Debug.LogError("No Cannon script was found on any parent of Cannon Arrow's Object.  Please ensure there is a Cannon script.");
			}
		}
	}


}
