﻿using UnityEngine;
using System.Collections;

public class EscButtonToClose : MonoBehaviour {

	public GameObject mEscapeMenu;

	public void Start(){

		mEscapeMenu = GameObject.FindGameObjectWithTag ("EscapeMenu");
		mEscapeMenu.SetActive (false);

		if (mEscapeMenu.GetComponent<OpenOrCloseMenu> () == null) {

			Debug.LogWarning("NoOpenOrCloseMenu script on escape menu");
		}
	}
	
	// Update is called once per frame
	void Update () {
	

		if (Input.GetKey (KeyCode.Escape)) {
			mEscapeMenu.GetComponent<OpenOrCloseMenu>().Open();
		}
	}
}
