﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BotlingTrail : UnitySingleton<BotlingTrail>
{	
	private const float UpdateFrequency = .25f;

	private Material lineMaterial;
	private float nextUpdateTime;
	private Dictionary<Transform, List<Vector3>> pointsToTrack;
	private Dictionary<Transform, LineRenderer> lineRenderers;

	// Use this for initialization
	void Awake() {
		this.pointsToTrack = new Dictionary<Transform, List<Vector3>>();
		this.lineRenderers = new Dictionary<Transform, LineRenderer>();

		this.lineMaterial = Resources.Load<Material>("BotlingTrail");
		this.nextUpdateTime = Time.realtimeSinceStartup + BotlingTrail.UpdateFrequency;
	}

	void Update()
	{
		if(Time.realtimeSinceStartup < this.nextUpdateTime) {
			return;
		}

		this.nextUpdateTime = Time.realtimeSinceStartup + BotlingTrail.UpdateFrequency;

		foreach(KeyValuePair<Transform, List<Vector3>> kvp in this.pointsToTrack) {
			if(kvp.Key != null){
				kvp.Value.Add(kvp.Key.position);
			}
		}
	}
	
	public void AddBotling(Transform botling) {
		this.pointsToTrack[botling] = new List<Vector3>();

		if(this.lineRenderers.ContainsKey(botling)) {
			GameObject.Destroy(this.lineRenderers[botling].gameObject);
		}

		this.lineRenderers[botling] = this.CreateLineRender();
	}

	public void EndTrail(Transform botling) {
		if(this.pointsToTrack.ContainsKey(botling)) {
			this.pointsToTrack[botling].Add(botling.position);
		}
	}

	public void Reset() {
		foreach(LineRenderer lr in this.lineRenderers.Values) {
			GameObject.Destroy(lr.gameObject);
		}

		this.pointsToTrack = new Dictionary<Transform, List<Vector3>>();
		this.lineRenderers = new Dictionary<Transform, LineRenderer>();
	}

	public void EnableRendering() {
		foreach(Transform botling in this.pointsToTrack.Keys) {
			this.lineRenderers[botling].enabled = true;
			this.lineRenderers[botling].SetVertexCount(this.pointsToTrack[botling].Count);
			for(int i = 0; i < this.pointsToTrack[botling].Count; ++i) {
				this.lineRenderers[botling].SetPosition(i, this.pointsToTrack[botling][i]);
			}
		}
	}

	public void DisableRendering() {
		foreach(LineRenderer lr in this.lineRenderers.Values) {
			lr.enabled = false;
		}
	}

	private LineRenderer CreateLineRender() {
		GameObject obj = new GameObject();
		obj.transform.parent = this.transform;

		LineRenderer render = obj.AddComponent<LineRenderer>();
		render.material = this.lineMaterial;
		render.SetWidth(.1f, .1f);

		return render;
	}
}
