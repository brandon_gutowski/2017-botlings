﻿using UnityEngine;
using System.Collections;

public class ModelTiler : MonoBehaviour {

	public int xTile = 1;
	public int yTile = 1;
	public string[] materialTypes;
	public Texture[] textures;
	

	public string modelName = "";

	void Awake ()
	{
		if(modelName != "")
		{
			this.gameObject.AddComponent<MeshRenderer>();
			this.gameObject.AddComponent<MeshFilter>();

			/*foreach(MeshFilter filter in GO.GetComponents<MeshFilter>())
			{
				print("SADSA" + filter.mesh.name);
			}*/

			//mesh =  GO.GetComponent<MeshFilter>().mesh;

			//tileMat.mainTextureScale = new Vector2(xTile, yTile);
			this.GetComponent<MeshRenderer>().materials = (Resources.Load("Models/" + modelName) as GameObject).GetComponent<MeshRenderer>().materials;
			this.GetComponent<MeshFilter>().mesh = (Resources.Load("Models/" + modelName) as GameObject).GetComponent<MeshFilter>().mesh;

			this.transform.eulerAngles = new Vector3(90, -180, 0);
//			this.GetComponent<MeshRenderer>() = GO.GetComponent<MeshRenderer>();
		}
	}
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
