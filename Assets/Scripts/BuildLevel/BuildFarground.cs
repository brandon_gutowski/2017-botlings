﻿#if UNITY_EDITOR
using UnityEngine;
using System.Collections;

//This script can be improved upon, expicially when taking the size of the level and location of windows into account. Lack of time until relase lead to a simpler option as seen here
public class BuildFarground : MonoBehaviour {
		public Vector3 spawnPosition = new Vector3(15, 8, 20);


	public void Build(){

		GameObject fargroundStorage;

		if (UnpackLevel.mID < 20) {
			fargroundStorage = Resources.Load ("Prefabs/Background/Farground Day")  as GameObject;
		} else {
			fargroundStorage = Resources.Load ("Prefabs/Background/Farground Night")  as GameObject;
		}

		Instantiate(fargroundStorage, spawnPosition, Quaternion.identity);

	}
}
#endif
