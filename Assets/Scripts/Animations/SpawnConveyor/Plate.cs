using UnityEngine;
using System.Collections;

public class Plate : MonoBehaviour {
	public delegate void PlateEvent(Plate plate);
	public event PlateEvent OnComplete;
	//public event PlateEvent OnDestroy;

	public Transform pivot;
	public Transform defaultParent;
	public float moveSpeed;
	public bool isFree = true;
	public bool isLive = false;
	public bool isActive = false;
	public bool isInit = false;

	public Vector3 increment;
	public int plateNum;
	public float originY;
	public Vector3 originRotation;
	public int adjustmentRotation = -90;
	public SpawnerConveyor.SpawnState spawnState;

	// Use this for initialization
	void Start () {
		//increment = Vector3.forward * moveSpeed;
	}

	public void Init()
	{
		if(!isInit)
			originRotation = this.transform.localEulerAngles;
		else
		{}

		originY = originRotation.y;
	}
	
	// Update is called once per frame
	void Update () {
		if(isActive)
		{
			if(isFree && isLive)
				movePlate();
		}
	}

	void movePlate() {
		this.transform.position += increment *  Time.deltaTime;
	}
	
	void OnTriggerEnter(Collider collider) {
		//print("Collider Tag = " + collider.tag);
		if(collider.tag == "RotoTrigger")
		{
			isFree = false;
			this.transform.parent = pivot.transform;
			//Destroy(this.gameObject);
		}
		else if(collider.tag == "Ender1" && isFree == false)
		{
			originRotation.y = adjustmentRotation;
			if(spawnState == SpawnerConveyor.SpawnState.Spawn)
			{
				if(adjustmentRotation < 0)
					increment = this.transform.parent.transform.parent.right * -moveSpeed;
				else
					increment = this.transform.parent.transform.parent.right * moveSpeed;
			}
			else
			{
				if(adjustmentRotation < 0)
					increment = this.transform.parent.transform.parent.forward * moveSpeed;
				else
					increment = this.transform.parent.transform.parent.forward * -moveSpeed;
			}

			this.transform.eulerAngles = originRotation;
			this.transform.parent = defaultParent;
			isFree = true;
		}
		else if(collider.tag == "Ender2")
		{
			this.transform.parent = defaultParent;

			OnComplete(this);

			if(isInit)
			{
//				OnDestroy(this);
//				Destroy(this);
			}
		}
	}

	public void Reset(Vector3 restartPosition, Vector3 resetDirection) {
		this.transform.position = restartPosition;
		originRotation.y = originY;
		this.transform.localEulerAngles = originRotation;
		increment = resetDirection * moveSpeed;
		isLive = false;
		isFree = true;

		this.transform.parent = defaultParent;
	}
}
