﻿using UnityEngine;
using System.Collections;

public class CollectorConveyorStart : MonoBehaviour {

	public int mConveyorSpeedEnd;
	public float mConveyorSpeedincriment;
	
	private float mCurrentSpeed = 2;
	
	//void OnTriggerEnter(Collider collider){
	//}

	void OnTriggerStay(Collider collider){
		
		if(collider.tag == "Robot"){
				
			collider.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
			collider.GetComponent<Rigidbody>().constraints -= RigidbodyConstraints.FreezePositionX;
			collider.GetComponent<Rigidbody>().constraints -= RigidbodyConstraints.FreezePositionY;
			
			
			mCurrentSpeed -= mCurrentSpeed * Time.deltaTime;
			
			if(mCurrentSpeed <= 1.2f * mConveyorSpeedEnd){
				mCurrentSpeed = 1.2f * mConveyorSpeedEnd;
			}
			
			collider.rigidbody.velocity = collider.transform.right * mCurrentSpeed;
		}
		
	}

	void OnTriggerExit(Collider collider){
		if(collider.tag == "Robot"){
			collider.GetComponent<Robot>().mGoingForwardOnSpawner = false;
		}
	}
}
