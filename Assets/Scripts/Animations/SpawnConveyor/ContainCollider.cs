﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class ContainCollider : MonoBehaviour {
	public delegate void ContainColliderEvent(Collider collider);
	public event ContainColliderEvent OnEnter_Trigger;
	public event ContainColliderEvent OnExit_Trigger;

	private List<Transform> _containList = new List<Transform>();
	public List<Transform> containList { get { return _containList; } }
	public int Count  {get { return _containList.Count; } }
	int lastCount;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (_containList.Contains (null))
			_containList = _containList.Where (a => a != null).ToList();
	}
	
	void OnTriggerEnter(Collider collider)
	{
		_containList.Add (collider.transform);

		if (OnEnter_Trigger != null)
			OnEnter_Trigger (collider);

	}
	
	void OnTriggerExit(Collider collider)
	{
		_containList.Remove (collider.transform);

		if (OnExit_Trigger != null)
			OnExit_Trigger (collider);

	}
}
