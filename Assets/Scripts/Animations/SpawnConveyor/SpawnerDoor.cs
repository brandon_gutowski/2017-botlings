﻿using UnityEngine;
using System.Collections;

public class SpawnerDoor : MonoBehaviour {

	public Transform door;
	public float mDoorSpeed;
	public float mDoorRaiseHeight;
	public float mSingleDoorOpenTime;

	private Vector3 mOpenDoorPos;
	private Vector3 mRestDoorPos;

	private float mCurrentTime = 0.0f;
	private float mTotalDoorOpenTime;
	[SerializeField] private bool mIsDoorOpen = false;
	[SerializeField] private bool mLockDoorOpen = false;

	void Start(){
		
		mRestDoorPos = door.localPosition;
		mOpenDoorPos = mRestDoorPos;
		mOpenDoorPos.y = mDoorRaiseHeight;
	}

	void Update(){

		if(mIsDoorOpen){
			mCurrentTime += Time.deltaTime;
			AnimateDoor(true);

			if(mCurrentTime > mTotalDoorOpenTime){
				mCurrentTime = 0f;
				mTotalDoorOpenTime = 0f;
				if(!mLockDoorOpen)	mIsDoorOpen = false;
			}
		}else{
			AnimateDoor(false);
		}
	}

	public void OpenDoor(){

		if(!mIsDoorOpen){

			mTotalDoorOpenTime += mSingleDoorOpenTime;
			mIsDoorOpen = true;

		}
	}


	void AnimateDoor (bool openDoor){
		if (openDoor)
			door.localPosition = Vector3.Slerp (door.localPosition, mOpenDoorPos, Time.deltaTime * mDoorSpeed);
		else
			door.localPosition = Vector3.Slerp (door.localPosition, mRestDoorPos, Time.deltaTime * mDoorSpeed);

	}
}
