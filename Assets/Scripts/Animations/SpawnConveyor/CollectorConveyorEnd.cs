﻿using UnityEngine;
using System.Collections;

public class CollectorConveyorEnd : MonoBehaviour {

	public int mConveyorSpeed;
	
	void OnTriggerStay(Collider collider){
		
		if(collider.tag == "Robot"){
			if(collider.GetComponent<Robot>().mGoingForwardOnSpawner){
				collider.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
				collider.GetComponent<Rigidbody>().constraints -= RigidbodyConstraints.FreezePositionZ;
				collider.rigidbody.velocity = collider.transform.right * mConveyorSpeed;
			}
		}
		
	}

	void OnTriggerExit(Collider collider){
		if(collider.tag == "Robot"){
			Destroy (collider.gameObject);
		}
		
	}
}
