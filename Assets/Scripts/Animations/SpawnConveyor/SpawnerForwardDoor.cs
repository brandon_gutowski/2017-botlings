﻿using UnityEngine;
using System.Collections;

public class SpawnerForwardDoor : MonoBehaviour {

	public int mConveyorSpeed;

	void OnTriggerStay(Collider collider){

		if(collider.tag == "Robot"){
			collider.rigidbody.velocity = collider.transform.right * mConveyorSpeed;

		}

	}

	void OnTriggerEnter(Collider collider){
		if(collider.tag == "Robot"){

			collider.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
			collider.GetComponent<Rigidbody>().constraints -= RigidbodyConstraints.FreezePositionZ;
			collider.GetComponent<Robot>().mGoingForwardOnSpawner = true;	
		}

	}

	void OnTriggerExit(Collider collider){
		if(collider.tag == "Robot"){
			collider.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
			collider.GetComponent<Robot>().mGoingForwardOnSpawner = false;	
		}
		
	}
}
