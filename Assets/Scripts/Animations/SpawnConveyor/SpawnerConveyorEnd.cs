﻿using UnityEngine;
using System.Collections;

public class SpawnerConveyorEnd : MonoBehaviour {
	
	public int mConveyorSpeedEnd;
	public float mConveyorSpeedincriment;
	public Vector3 mDirection;
	public int mLayerToChangeTo;

	private int mNormalRobotLayer;
	private float mCurrentSpeed = 2;

	//void OnTriggerEnter(Collider collider){
	//}

	void OnTriggerExit(Collider collider){
		
		if(collider.tag == "Robot"){
			collider.gameObject.layer = mLayerToChangeTo;
		}
	}

	void OnTriggerStay(Collider collider){

		if(collider.tag == "Robot"){
			if(collider.GetComponent<Robot>().mGoingForwardOnSpawner){

				collider.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
				collider.GetComponent<Rigidbody>().constraints -= RigidbodyConstraints.FreezePositionX;
				collider.GetComponent<Rigidbody>().constraints -= RigidbodyConstraints.FreezePositionY;

				mCurrentSpeed += mCurrentSpeed * Time.deltaTime;

				if(mCurrentSpeed >= 0.9f* mConveyorSpeedEnd){
					mCurrentSpeed = 0.9f* mConveyorSpeedEnd;
				}

				collider.rigidbody.velocity = collider.transform.right * mCurrentSpeed;
			}			
		}

	}
}
