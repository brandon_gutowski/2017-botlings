using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

public class SpawnerConveyor : MonoBehaviour {
	public enum SpawnState
	{
		Spawn,
		Collect
	}

	public Transform plate;
	public Transform plateSpawner;
	
	public Transform pivot;
	public BoxCollider pivotTrigger;
	
	public bool isRight = true;
	public SpawnState spawnState = SpawnState.Spawn;
	public string botName;
	public const int maxPlateNum = 17;
	public float plateSpeed;
	public float pivotSpeed;
	public float spawnBuffer;
	public float collectBuffer;
	public ContainCollider areaCollider;

	private float dist;


	private bool _isActive = false;
	private bool isActive
	{
		get{return _isActive;}
		set
		{
			_isActive = value;
			PlateList.ForEach( a => a.GetComponent<Plate>().isActive = value);
		}
	}

	private Transform currPlate;
	private float buffer = 0;
	private Vector3 position;
	private Quaternion rotation;

	List<Transform> PlateList = new List<Transform>();

	// Use this for initialization
	void Start () {

		if (spawnState == SpawnState.Spawn)
		{
			buffer = spawnBuffer;
			rotation = plate.rotation;
			plateSpawner = transform.FindChild("Spawners").FindChild("SpawnPoint_Spawn");
			transform.FindChild("Triggers").FindChild("RecycleTrigger_Spawn").gameObject.SetActive(true);
			transform.FindChild("Triggers").FindChild("UnPivotTrigger_Spawn").gameObject.SetActive(true);
		}
		else 
		{
			buffer = collectBuffer;
			rotation = Quaternion.Euler(new Vector3( 0, 90, 0));
			plateSpawner = transform.FindChild("Spawners").FindChild("SpawnPoint_Collect");
			transform.FindChild("Triggers").FindChild("RecycleTrigger_Collect").gameObject.SetActive(true);
			transform.FindChild("Triggers").FindChild("UnPivotTrigger_Collect").gameObject.SetActive(true);
			pivotSpeed *= -1;
		}

		PreloadPlates();

//		for(int i = 0; i < maxPlateNum; ++i)
//		{
//			SpawnPlate(i);
//		}
		
		if(!isRight)
			pivotSpeed *= -1;

		PlateList[0].GetComponent<Plate>().isLive = true;
		currPlate = PlateList[0];

		this.areaCollider.OnEnter_Trigger += CheckActive;
		this.areaCollider.OnExit_Trigger += CheckInactive;

	}
	
	// Update is called once per frame
	void Update () {
		if(isActive)
		{	
			rotatePivot();
			ManagePlates();
		}

//		if (Input.GetKeyDown(KeyCode.Space))
//			CreatePositionXML();
	}

	void rotatePivot() {
		pivot.localEulerAngles += new Vector3(0, pivotSpeed, 0);
	}

	void ManagePlates() {
		if(currPlate != null)
			dist =  Mathf.Abs(plateSpawner.position.z - currPlate.position.z);

		if(dist >= buffer || currPlate == null)
		{
			currPlate = PlateList.FirstOrDefault(a => a.GetComponent<Plate>().isLive == false);
			if(currPlate != null)
				currPlate.GetComponent<Plate>().isLive = _isActive;
		}
	}

	void SpawnPlate(int plateNumber)
	{
		var newPlate = Instantiate(plate, plateSpawner.position, rotation) as Transform;

		newPlate.GetComponent<Plate>().isInit = false;

		newPlate.position = plateSpawner.position;

		newPlate.GetComponent<Plate>().spawnState = spawnState;
		newPlate.GetComponent<Plate>().plateNum = plateNumber;
		newPlate.GetComponent<Plate>().pivot = pivot;
		newPlate.GetComponent<Plate>().isFree = true;
		newPlate.GetComponent<Plate>().moveSpeed = plateSpeed;
		newPlate.GetComponent<Plate>().defaultParent = plateSpawner;

		if (spawnState == SpawnState.Spawn)
			newPlate.GetComponent<Plate>().increment = this.transform.forward * plateSpeed;
		else
		{
			if(!isRight)
			{
				newPlate.GetComponent<Plate>().increment = this.transform.right * plateSpeed;
				newPlate.GetComponent<Plate>().adjustmentRotation = 0;
			}
				else
			{
				newPlate.GetComponent<Plate>().increment = this.transform.right * -plateSpeed;
				newPlate.GetComponent<Plate>().adjustmentRotation = 180;
			}
		}

		if(!isRight)
			newPlate.GetComponent<Plate>().adjustmentRotation *= -1;

		newPlate.GetComponent<Plate>().OnComplete += RecyclePlate;

		newPlate.transform.parent = plateSpawner;
		PlateList.Add(newPlate);
	}

	public void RecyclePlate (Plate tempPlate)
	{
		if (spawnState == SpawnState.Spawn)
			tempPlate.Reset (plateSpawner.position, this.transform.forward);
		else
		{
			if(!isRight)
				tempPlate.Reset (plateSpawner.position, this.transform.right);
			else
				tempPlate.Reset (plateSpawner.position, -1f * this.transform.right);
		}
	}
	
	void CheckActive (Collider collider)
	{
		if (!isActive) 
		{
			if(areaCollider.containList.FirstOrDefault(a => a.name == botName) != null)
			{
				isActive = true;
			}
		}
	}
	
	void CheckInactive (Collider collider)
	{
		if (isActive)
		{
			if (areaCollider.containList.FirstOrDefault (a => a.name == botName) == null)
			{
				isActive = false;
			}
		}
	}

	public void PreloadPlates()
	{
		XDocument data = GetPreloadData(isRight, spawnState);
		List<XElement> loadedPlates = data.Descendants("data").Descendants("PlateSpecs").ToList();
	
		loadedPlates.ForEach(a => LoadPlate(a));

		if(isRight && spawnState == SpawnState.Spawn)
			pivot.localEulerAngles += new Vector3(0, 145, 0);
		else if(isRight && spawnState == SpawnState.Collect)
			pivot.localEulerAngles += new Vector3(0, -95, 0);
		else if (!isRight && spawnState == SpawnState.Collect)
			pivot.localEulerAngles += new Vector3(0, 65, 0);
	}
	
	void LoadPlate (XElement plateElement)
	{
		var newPlate = Instantiate(plate, plateSpawner.position, rotation) as Transform;

		newPlate.GetComponent<Plate>().isInit = true;

		if(plateElement.Element("Parent").Value.ToString() == "SpinAnchor")
			newPlate.transform.parent = pivot;
		else
			newPlate.transform.parent = plateSpawner;
		
		Vector3 tempPosition = new Vector3(float.Parse(plateElement.Element("X").Value.ToString()),
		                                   float.Parse(plateElement.Element("Y").Value.ToString()),
		                                   float.Parse(plateElement.Element("Z").Value.ToString()));
		
		newPlate.localPosition = tempPosition;
		
		tempPosition = new Vector3(0, float.Parse(plateElement.Element("RotoY").Value.ToString()), 0);

//		if(isRight)
//			tempPosition *= -1f;
		
		newPlate.localEulerAngles = tempPosition;
		
		newPlate.GetComponent<Plate>().spawnState = spawnState;
		newPlate.GetComponent<Plate>().plateNum = int.Parse(plateElement.Element("PlateNum").Value.ToString());
		newPlate.GetComponent<Plate>().pivot = pivot;

		newPlate.GetComponent<Plate>().isFree = bool.Parse(plateElement.Element("IsFree").Value.ToString());
		newPlate.GetComponent<Plate>().isLive = bool.Parse(plateElement.Element("IsLive").Value.ToString());

		newPlate.GetComponent<Plate>().moveSpeed = plateSpeed;
		newPlate.GetComponent<Plate>().defaultParent = plateSpawner;
		
		newPlate.GetComponent<Plate>().increment = new Vector3(float.Parse(plateElement.Element("IncX").Value.ToString()), 
		                                                       float.Parse(plateElement.Element("IncY").Value.ToString()), 
		                                                       float.Parse(plateElement.Element("IncZ").Value.ToString()));

		int absModifier = (int)Mathf.Abs(this.transform.localEulerAngles.y / 180);

		if(absModifier == 1)
			newPlate.GetComponent<Plate>().increment *= -1;

		newPlate.GetComponent<Plate>().originRotation = new Vector3(float.Parse(plateElement.Element("OriginRotoX").Value.ToString()),
		                                                            float.Parse(plateElement.Element("OriginRotoY").Value.ToString()),
		                                                            float.Parse(plateElement.Element("OriginRotoZ").Value.ToString()));

		newPlate.GetComponent<Plate>().adjustmentRotation = int.Parse(plateElement.Element("AdjRoto").Value.ToString());
		newPlate.GetComponent<Plate>().originY = float.Parse(plateElement.Element("OriginY").Value.ToString());

		newPlate.GetComponent<Plate>().OnComplete += RecyclePlate;

		PlateList.Add(newPlate);
	}
	
	XDocument GetPreloadData (bool isRight, SpawnState spawnState)
	{
		switch(spawnState)
		{
			case SpawnState.Collect:
				if(isRight)
				return XDocument.Load("Assets/Resources/XML/RightCollectPlates.xml");
				else
				return XDocument.Load("Assets/Resources/XML/LeftCollectPlates.xml");
			case SpawnState.Spawn:
				if(isRight)
				return XDocument.Load("Assets/Resources/XML/RightSpawnPlates.xml");
				else
				return XDocument.Load("Assets/Resources/XML/LeftSpawnPlates.xml");
		}

		return new XDocument();
	}
	
	void CreatePositionXML()
	{
		XDocument doc = new XDocument(new XElement("data"));
		string fileName = "";

		foreach(Transform child in plateSpawner)
		{
			doc.Element("data").Add(CreatePositionElement(child));	
		}
		
		foreach(Transform child in pivot)
		{
			doc.Element("data").Add(CreatePositionElement(child));	
		}

		if(isRight)
			fileName = "Right";
		else
			fileName = "Left";

		doc.Save("Assets/Resources/XML/" + fileName + spawnState + "Plates.xml");
	}
	
	XElement CreatePositionElement(Transform loadedPlate)
	{
		XElement plateElement = new XElement("PlateSpecs",
		             new XElement("X", loadedPlate.localPosition.x),
                     new XElement("Y", loadedPlate.localPosition.y),
                     new XElement("Z", loadedPlate.localPosition.z),
                     new XElement("RotoY", loadedPlate.localEulerAngles.y),
                     new XElement("IsFree", loadedPlate.GetComponent<Plate>().isFree),
                     new XElement("IsLive", loadedPlate.GetComponent<Plate>().isLive),
                     new XElement("IncX", loadedPlate.GetComponent<Plate>().increment.x),
                     new XElement("IncY", loadedPlate.GetComponent<Plate>().increment.y),
                     new XElement("IncZ", loadedPlate.GetComponent<Plate>().increment.z),
                     new XElement("PlateNum", loadedPlate.GetComponent<Plate>().plateNum),
                     new XElement("OriginY", loadedPlate.GetComponent<Plate>().originY),
                     new XElement("OriginRotoX", loadedPlate.GetComponent<Plate>().originRotation.x),
                     new XElement("OriginRotoY", loadedPlate.GetComponent<Plate>().originRotation.y),
                     new XElement("OriginRotoZ", loadedPlate.GetComponent<Plate>().originRotation.z),
                     new XElement("AdjRoto", loadedPlate.GetComponent<Plate>().adjustmentRotation),
                     new XElement("Parent", loadedPlate.transform.parent.name)
     	);

		return plateElement;
	}
}
