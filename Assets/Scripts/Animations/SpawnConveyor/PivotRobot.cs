﻿using UnityEngine;
using System.Collections;

public class PivotRobot : MonoBehaviour {

	public bool mIsFacingRight;
	public Transform mPivot;
	public float mPivotSpeed;

	void OnTriggerEnter(Collider collider){

		if(collider.tag == "Robot"){
			collider.transform.parent = mPivot;
		}
	}

	void OnTriggerExit(Collider collider){
		if(collider.tag == "Robot"){
			collider.transform.parent = null;
			collider.GetComponent<Robot>().mGoingForwardOnSpawner = true;
		}
	}

	void OnTriggerStay(Collider collider){
		
		if(collider.tag == "Robot"){
			if(!collider.GetComponent<Robot>().mGoingForwardOnSpawner){
				collider.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePosition;

				RotateAroundPivot();

			}
		}
	}

	void RotateAroundPivot(){

		if(mIsFacingRight){
			mPivot.localEulerAngles += new Vector3(0, -mPivotSpeed, 0);

		}else{
			mPivot.localEulerAngles += new Vector3(0, mPivotSpeed, 0);

		}
	}

}
