﻿using UnityEngine;
using System.Collections;

public class TextureSlider : MonoBehaviour {

	public float mOffsetX;
	
	private Material[] mMaterialsArray;
	private Vector2 mOffset;
	
	void Start () {
		mMaterialsArray = this.renderer.materials;
	}

	void Update () {
		ShiftMaterialsByXOffset ();
	}

	void ShiftMaterialsByXOffset(){
		mOffset.x -= mOffsetX * Time.deltaTime;
		mOffset.y = 0;
		
		mMaterialsArray[0].SetTextureOffset("_MainTex", mOffset);
		
		if(mMaterialsArray.Length > 2){
			mMaterialsArray[2].SetTextureOffset("_MainTex", mOffset);
		}
	}
}
