﻿using UnityEngine;
using System.Collections;

public class Cannon : MonoBehaviour {

	public int mStoredRobots;
	public int mLaunchVelocity;
	public int mMaxStorage;
	public float mRotationSpeed;
	public float[] mMaxRotationAngleFloor;
	public AudioContainer mCannonAudioContainer;
	public VFXContainter mVFXContainer;
	public bool isRotating = false;

	//Models
	[SerializeField]private GameObject mFloorArmature;
	[SerializeField]private GameObject mFloorGameObject;
	
	[SerializeField]private GameObject mLaunchPoint;
	[SerializeField]private GameObject mRobot;

	private Vector3 mRotation = new Vector3( 0,0,0);
	private int mRotationDirection = 1;
	private Vector3 mRestRotation = new Vector3 (0, 0, 0);

	void OnTriggerEnter(Collider collider){

		if(collider.tag == "Robot"){
			if(mStoredRobots <= mMaxStorage){
				Destroy(collider.gameObject);
				mStoredRobots++;
			}
		}
	}

	void Start(){

		HideAndRevealTheMount mHideReveal = this.GetComponentInParent<HideAndRevealTheMount>();

		if (mHideReveal != null){
			if(mHideReveal.mIsOnFloor){
				SetOnFloor();
			}
		}
	}

	void Update () {
		
		if (mStoredRobots > 0) {
			RotateCannon ();
		}
		else{
			isRotating = false;
		}
	}

	//Robot should launch out of the cannon in an arch based on the cannon direction and launch velocity
	public void FireRobot(){

		if (mRobot != null) {
			if (mStoredRobots > 0) {

				GameObject launchedRobot = Instantiate (mRobot, mLaunchPoint.transform.position, Quaternion.identity) as GameObject;
				Vector3 launchVector  = (mLaunchPoint.transform.position - transform.position).normalized;
			
				launchedRobot.rigidbody.velocity = mLaunchVelocity * launchVector;
				mStoredRobots--;

				if (mCannonAudioContainer != null) {
					mCannonAudioContainer.PlayRobotInterractionEffect ();
				} else {
					Debug.LogWarning ("Audio Container is missing and you are trying to play a sound effect on" + gameObject.name);
				}

				if (mVFXContainer != null) {
					mVFXContainer.PlayPlayerInterraction (0.0f);
				} else {
					Debug.LogWarning ("VFXContainer is missing and you are trying to play an effect on" + gameObject.name);
				}
			}
		} else {
			Debug.LogWarning("mRobot is null on" + gameObject.name);
		}
	}

	void RotateCannon(){
		if (mRotation.z <= mMaxRotationAngleFloor [0])
			mRotationDirection = -1;
		if (mRotation.z  >= mMaxRotationAngleFloor [1])
			mRotationDirection = 1;

		mRotation.z -= mRotationDirection * mRotationSpeed;

		if (mFloorArmature != null) {
			mFloorArmature.transform.Rotate (0, 0, mRotationDirection * mRotationSpeed * -1);
		} else {
			Debug.LogWarning ("mFloorArmature is null.  Cannon will not rotate: " + gameObject.name);
		}

		isRotating = true;
	}

	//Sets the model and resting rotation to they of the floor configuration
	public void SetOnFloor(){

		if (mFloorGameObject != null) {
			mFloorGameObject.SetActive (true);
		} else {
			Debug.LogWarning ("mFloorArmature is null: " + gameObject.name);
		}

		/*if(mFloorArmature != null){
			mFloorArmature.transform.eulerAngles = mFloorRestRotation;
		} else {
			Debug.LogWarning("mFloorArmature is null.  Cannon will not rotate: " + gameObject.name);
		}*/

		mRotation = mRestRotation;
		

	}

}
