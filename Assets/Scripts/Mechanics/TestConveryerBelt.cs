using UnityEngine;
using System.Collections;

public class TestConveryerBelt : MonoBehaviour {

	public int mConveyerSpeed;

	private Vector3 mCurrentVelocity;
	private Vector3 mMoveDirection;
	
	void Start(){

		UpdateMovingDirection();
	}
	
	void OnTriggerStay(Collider collider){

		if(collider.gameObject.tag == "Robot"){

			UpdateMovingDirection();
			ApplyVelocityToRobot(collider);
		}
	}

	void UpdateMovingDirection(){

		mMoveDirection = new Vector3(0,0,0);
		mMoveDirection.x = transform.position.x - transform.parent.transform.position.x;
		mMoveDirection = mMoveDirection.normalized;
	
	}

	void ApplyVelocityToRobot(Collider robotCollider){

		mCurrentVelocity = robotCollider.attachedRigidbody.velocity;
		robotCollider.attachedRigidbody.velocity = new Vector3 (mMoveDirection.x * mConveyerSpeed, mCurrentVelocity.y, 0);
	}
}
