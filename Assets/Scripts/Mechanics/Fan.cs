﻿using UnityEngine;
using System.Collections;

public class Fan : MonoBehaviour {
	
	public float mFanSpeed;

	private Vector3 mFanDirection;

	void OnTriggerStay(Collider collider){

		//For Fan direction to function properly, this script must be placed on the gameobject where the fan's interaction collider is and it must be a child of the fan object
		mFanDirection = (transform.position - transform.parent.transform.position).normalized;

		if(collider.gameObject.tag == "Robot"){
			collider.attachedRigidbody.AddForce (mFanDirection * mFanSpeed);
		}
	}
}
