﻿using UnityEngine;
using System.Collections;

public class SwitchMechanic : MonoBehaviour {

	public DoorMechanic mDoorScript;
	public Color mColor;
	public AudioContainer mSwitchAudioContainer;
	[HideInInspector] public int mButtonID;

	[SerializeField] private Light mLightOnThisObject;
	private float mLastCall;
	private float mCallInterval = 0.5f;

	void Awake(){

		mDoorScript.AddSwitch(mButtonID);
		gameObject.renderer.material.color = new Color(mColor.r, mColor.g, mColor.b, 113f/256f);

		if(mLightOnThisObject != null){
			mLightOnThisObject.color = mColor;
		}

		mLastCall = Time.time;
	}

	void OnTriggerEnter(Collider collider){

		if(collider.tag == "Robot"){
			ActivateDoor();
			if(Time.time >= mLastCall + mCallInterval){
				mLastCall = Time.time;
			}
		}
	}

	void ActivateDoor(){
		mDoorScript.Switched(mButtonID);

		if(mSwitchAudioContainer != null){
			mSwitchAudioContainer.PlayRobotInterractionEffect();
		}

		this.gameObject.SetActive(false);
	}
}
