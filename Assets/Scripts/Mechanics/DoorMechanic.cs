﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DoorMechanic : MonoBehaviour {

	public Color mColor;
	[SerializeField] private Animation mActivationAnimation;
	[SerializeField] private AudioContainer mDoorAudioContainer;
	[SerializeField] private Color mDangerColor;
	[SerializeField] private float mFlashTime;
	
	private List<int> mSwitchList = new List<int>();

	void Awake(){
		for(int i = 0; i < this.renderer.materials.Length; i++){
			this.renderer.materials[i].color = mColor;
		}
	}

	//This exsists so that a door can have mulitple switches associated with it
	public void AddSwitch(int buttonID){
		mSwitchList.Add (buttonID);
	}

	//Called whenever the associated switch is activated, this removes it from the list of associated switches
	public void Switched(int buttonID){
	
		mSwitchList.Remove (buttonID);

		if(mSwitchList.Count <= 0){
			TriggerEvent();
		}
	}

	private void TriggerEvent(){
		if(mActivationAnimation != null){
			mActivationAnimation.Play();
		}
		if(mDoorAudioContainer != null){
			mDoorAudioContainer.PlayRobotInterractionEffect();
		}
		this.gameObject.SetActive(false);
	}

	void OnTriggerEnter(Collider collider){
		if(collider.tag == "Robot"){
			if(mDoorAudioContainer != null){
				mDoorAudioContainer.PlayRobotInterractionEffect();
			}

			StartCoroutine(FlashColor());

			collider.gameObject.GetComponent<RobotVisualEffect>().PlayExplosion();
			collider.enabled = false;
		}
	}

	IEnumerator FlashColor(){

		Material[] materials = this.renderer.materials;
		Color baseColor = this.renderer.material.color;

		foreach(Material m in materials){
			m.color = mDangerColor;
		}

		yield return new WaitForSeconds(mFlashTime);

		foreach(Material m in materials){
			m.color = baseColor;
		}
	}
}
