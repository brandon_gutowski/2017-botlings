﻿using UnityEngine;
using System.Collections;

public class EndGoal : MonoBehaviour {

	public AudioContainer mCollectorAudioContainer;
	public SpawnerDoor mSpawnerDoorScript;

	[SerializeField] private int mSpawnerLayer;

	void OnTriggerEnter(Collider collider){
		
		if(collider.gameObject.tag == "Robot"){
			EventHandler.CallRobotScored(collider.gameObject);
			collider.gameObject.layer = mSpawnerLayer;

			if(mSpawnerDoorScript != null){
				mSpawnerDoorScript.OpenDoor();
			}else{
				Debug.Log ("No SpawnerDoor script is attached to " + this.gameObject.name);
			}

			GivePlayerOneRobotBack();

			if(mCollectorAudioContainer != null){
				mCollectorAudioContainer.PlayRobotInterractionEffect();
			}
		}
	}

	//This was a design decision to allow players to be rewarded for efficiently thinking about their puzzle before sending a robot
	//Also allows the player to start the next level immediately, thus making them more likely to encounter ads.
	void GivePlayerOneRobotBack(){

		PlayerData.instance.mNumberOfRobots++;
		Camera.main.GetComponent<MainCameraGUI> ();
	}
}
