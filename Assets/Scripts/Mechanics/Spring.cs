using UnityEngine;
using System.Collections;

public class Spring : MonoBehaviour {
	
	public float mLaunchHeight;
	public float mLaunchDistance;
	public Animation mSpringAnimation;
	[Range (0.1f, 1.0f)]
	public float mSpeedCorrectionSmoothness = 0.6f;

	private float mXLaunchSpeed;
	private float mYLaunchSpeed;
	private Vector3 mRobotTravelDirection = new Vector3(0,0,0);

	void Awake(){
		//Animation isplayed at the beginning as a method to set the animation's position to the flattened, pre launch position
		if(mSpringAnimation != null){
			mSpringAnimation.Play();
		}
	}

	void Start(){

		CalculateLaunchSpeeds();

	}

	void OnTriggerEnter(Collider collider){
		if (collider.gameObject.tag == "Robot") {					
			collider.GetComponent<Robot>().mTimeToReachSpringLaunchPoint = SetLaunchTime(collider);
		}
	}

	void OnTriggerExit(Collider collider){
		if (collider.gameObject.tag == "Robot") {
			collider.GetComponent<Robot>().mHasLaunched = false;
		}
	}
	
	void OnTriggerStay(Collider collider){		
		if(collider.gameObject.tag == "Robot"){
			mRobotTravelDirection = collider.attachedRigidbody.velocity;
			CalculateLaunchSpeeds();
			
			if(!collider.GetComponent<Robot>().mHasLaunched){
				LaunchRobot(collider);

			}
		}
	}

	void CalculateLaunchSpeeds(){
		
		float gravity = Physics.gravity.magnitude;
		float totalFlightTime;
		
		mRobotTravelDirection.y = 0;
		mRobotTravelDirection.z = 0;
		mRobotTravelDirection = mRobotTravelDirection.normalized;
		
		mYLaunchSpeed = Mathf.Sqrt (2f *gravity *mLaunchHeight);
		totalFlightTime = 2f * mYLaunchSpeed/gravity;
		mXLaunchSpeed = mRobotTravelDirection.x * mLaunchDistance/totalFlightTime;
		
	}

	void LaunchRobot(Collider collider){
		
		collider.attachedRigidbody.velocity = new Vector3 (mXLaunchSpeed, mYLaunchSpeed, 0);
		
		FaceSpringInCorrectDirection();		
		
		if(gameObject.GetComponent<VFXContainter>() != null){
			gameObject.GetComponent<VFXContainter>().PlayRobotInterraction(0.0f);
			if(mSpringAnimation != null){
				mSpringAnimation.Play();
			}
		}
		
		if(gameObject.GetComponent<AudioContainer>() != null){
			gameObject.GetComponent<AudioContainer>().PlayRobotInterractionEffect();
		}
	}


	//This is set to delay the launch time of the robot so that it will always end up in the same position away from the center of the spring 
	// regardless of where it entered and how fast it was going
	float SetLaunchTime(Collider collider){

		mRobotTravelDirection = collider.attachedRigidbody.velocity;//This is velocity Initial

		float launchTime = 0;
		float ySize;

		if (this.GetComponent<BoxCollider> () != null) {
			ySize = this.GetComponent<BoxCollider> ().size.y;
			float yTravelDistance = ySize*2;
			float sqrtValue = (mRobotTravelDirection.y + 2f * Physics.gravity.y * yTravelDistance);
			float velocityFinal = Mathf.Sqrt(sqrtValue);
			launchTime = (velocityFinal - mRobotTravelDirection.y) / Physics.gravity.y; // (vf - vi)/a = t


		} else {
			Debug.LogError("No BoxCollider on " + this.gameObject.name);

		}

		return launchTime;
	}
	
	void FaceSpringInCorrectDirection(){

		Vector3 springFacingDirection = transform.parent.GetComponentInChildren<Animation>().transform.right;

		//transform.right need to be -1 for the animation to play right
		if(mRobotTravelDirection == transform.right){//Robot Going Right

			if(mRobotTravelDirection == springFacingDirection){//Spring facing left, turn around
				transform.parent.GetComponentInChildren<Animation>().gameObject.transform.Rotate( 0, 180, 0);
			}
		}else if(mRobotTravelDirection == springFacingDirection){//Robot going left spring not facing left, turn around

			transform.parent.GetComponentInChildren<Animation>().gameObject.transform.Rotate(0, 180, 0);
		}
	}


}
