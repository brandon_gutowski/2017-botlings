﻿using UnityEngine;
using System.Collections;

public class SpawnerModelManager : MonoBehaviour {
	
	public string mIncorrectModelName;

	//Whenever the level is reset and the spawner loaded back in, this function should be called
	public void Reset(){
		
		Destroy(transform.FindChild(mIncorrectModelName).gameObject);
	}

}