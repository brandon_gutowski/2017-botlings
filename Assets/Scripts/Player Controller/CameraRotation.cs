﻿using UnityEngine;
using System.Collections;

public class CameraRotation : MonoBehaviour {
	public Transform rotationTransform;
	public Vector3 rotationVector;
	
	void Start () {
		rotationVector = rotationTransform.eulerAngles;
	}

	void Update () {
		rotationVector.x += .5f;
		rotationTransform.eulerAngles = rotationVector;
	}
}
