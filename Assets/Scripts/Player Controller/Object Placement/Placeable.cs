using UnityEngine;
using System.Collections;

public class Placeable : MonoBehaviour 
{
	public float mRotationDegrees;
	public AudioSource mConstructionSoundEffect;
	[HideInInspector]public bool mHasBeenChargedForMount = false;
	[HideInInspector]public bool mIsOnFloor = false;
	[HideInInspector]public bool mIsOnWall = false;
	[HideInInspector]public bool mIsOnConveyor = false;

	[HideInInspector]public Vector3 mHomePosition;
	[HideInInspector]public bool mHaveHomePosition = false;
	public static float mPlacementDelayTime = 0.5f;

	public bool mReadIsAttachedToMouse{ get{ return mIsAttachedToMouse;}}
	public float TimeSinceCreation(){return mTimeSinceCreation;}

	private float mSnapSize;			
	private bool mIsAttachedToMouse = false;
	private Camera mCamera;
	private Transform mCameraTransform;
	private Transform mTransform;
	private float mTimeSinceCreation = 0.0f;
	private bool mBeingPlaced = false;

	[SerializeField] private ValidPlaceablePosition mValidPlaceablePositionInstance;
	[SerializeField] private PlaceableMovementTransitionMode mPlaceableMovementTransitionModeInstance;

	void Awake (){
		mCamera = Camera.main;
		mCameraTransform = mCamera.transform;
		mTransform = transform;

		if (mValidPlaceablePositionInstance == null) {
			Debug.LogWarning("No ValidPlaceablePosition Script on " + this.gameObject.name);
		}
		if (mPlaceableMovementTransitionModeInstance == null) {
			Debug.LogWarning("No PlaceableMovementTransitionMode Script on " + this.gameObject.name);
		}
	}

	void Start(){
		EventHandler.OnSFXToggle += UpdateSFXVolume;
	}

	void OnDestroy(){
		EventHandler.OnSFXToggle -= UpdateSFXVolume;
		EventHandler.CallPlaceableDeleted (this.transform);
		StopAllCoroutines();
	}
	
	void Update (){	
		mTimeSinceCreation += Time.deltaTime;
		
		if(mIsAttachedToMouse || mTimeSinceCreation < 0.1f){	
			Vector3 mousePosition = Input.mousePosition;
			mousePosition.z = mTransform.position.z - mCameraTransform.position.z;
			mousePosition = mCamera.ScreenToWorldPoint(mousePosition);

			gameObject.GetComponent<PlaceableMovementTransitionMode>().MakeTransparent();

			if(!mValidPlaceablePositionInstance.IsValidPosition()){
				mPlaceableMovementTransitionModeInstance.ChangeColor();
			}

			mTransform.position = this.CalculateNewPosition(mousePosition);	
		}
	}
	
	public void AttachToMouse(float snapSize){
		mSnapSize = snapSize;
		mIsAttachedToMouse = true;
		ToggleMechanicsScript(true);

		if (this.GetComponent<SphereCollider> () != null) {
			this.GetComponent<SphereCollider> ().enabled = false;
		}

		if(!mHaveHomePosition){
			mHomePosition = this.transform.position;

			mHaveHomePosition = true;
		}
	}

	private void ToggleMechanicsScript(bool state){
		if(gameObject.GetComponent<HideAndRevealTheMount>() != null){
			gameObject.GetComponent<HideAndRevealTheMount>().Attached(state);
		}

	}

	public void Place(Vector3 mouseLocation){		
		mIsAttachedToMouse = false;

		if(!gameObject.GetComponent<ValidPlaceablePosition>().mIsValidPosition){
			ReturnToHomePosition();
		}

		StartCoroutine(DelayPlacement());	
		mHaveHomePosition = false;
	}
		
	private Vector3 CalculateNewPosition(Vector2 mousePosition){

		if(this.mSnapSize == 0){
			return new Vector3(mousePosition.x, mousePosition.y, mTransform.position.z);
		}

		Vector2 remainder = new Vector2(mousePosition.x % mSnapSize, mousePosition.y % mSnapSize);
		Vector2 offset = new Vector2(0, 0);
		
		if(remainder.x >= mSnapSize / 2){
			offset.x = mSnapSize;
		}
		
		if(remainder.y >= mSnapSize / 2){
			offset.y = mSnapSize;
		}
		
		Vector3 newPosition = mTransform.position;
		newPosition.x = mousePosition.x - remainder.x + offset.x;
		newPosition.y = mousePosition.y - remainder.y + offset.y;
		
		return newPosition;
	}

	public void RotateCounterClockwise(){
		if(mRotationDegrees == 180){		
			transform.Rotate (0, mRotationDegrees, 0);		
		}
		else{
			transform.Rotate(0,0, mRotationDegrees);
		} 
	}

	void ReturnToHomePosition(){
		if( mHomePosition == PlayerObjectButtons.mDefaultHomePosition){
			mCamera.GetComponent<PlayerObjectButtons>().DeleteObject(gameObject.name);
			GameObject.Destroy(gameObject);
			EventHandler.CallDeleteTool();
			
		}
		else{
			gameObject.transform.position = mHomePosition;
			mHaveHomePosition = false;
			mPlaceableMovementTransitionModeInstance.MakeOpaque();
			mBeingPlaced = false;
			EventHandler.CallPlace();
			ToggleMechanicsScript(false);
		}
	}

	void OnTriggerStay(Collider collider){
		if (collider is BoxCollider) {
			if (mBeingPlaced && collider.tag == "Robot") {
					Destroy (collider.gameObject);
			}
		}
	}

	IEnumerator DelayPlacement(){

		mBeingPlaced = true;
		EventHandler.CallPlace();

		yield return new WaitForSeconds(mPlacementDelayTime);
		if(gameObject.GetComponent<ValidPlaceablePosition>().mIsValidPosition){

			mPlaceableMovementTransitionModeInstance.MakeOpaque();
			mBeingPlaced = false;

			ToggleMechanicsScript(false);
		}else{

			ReturnToHomePosition();
		}
	
		if (this.GetComponent<SphereCollider> () != null) {
			this.GetComponent<SphereCollider> ().enabled = true;
		}

		EventHandler.CallPlaceFinish (this.transform);
	}

	void UpdateSFXVolume(bool state){
		mConstructionSoundEffect.mute = !state;
	}
}
