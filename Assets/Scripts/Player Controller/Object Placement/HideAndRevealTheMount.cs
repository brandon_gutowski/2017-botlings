using UnityEngine;
using System.Collections;

public class HideAndRevealTheMount : MonoBehaviour
{
	[HideInInspector]public bool mIsOnFloor = true;

	private Collider mCurrentCollider;

	public void OnTriggerEnter(Collider collider){
		if (collider is BoxCollider) {
			if (collider.gameObject.tag == "LevelPiece") {
				if (collider.gameObject.GetComponent<HideWhenCovered> () != null) {
					mCurrentCollider = collider;
					
					if (gameObject.GetComponent<HideWhenCovered> () != null) {
						gameObject.GetComponent<HideWhenCovered> ().Hide (gameObject);
					}
				}
			}
		}
	}
	
	public void OnTriggerStay(Collider collider){	
		if (collider is BoxCollider) {

			Placeable thisPlaceableScript = gameObject.GetComponent<Placeable> ();

			if (collider.gameObject.name == "Floor") {
				this.mIsOnFloor = true;
				if (thisPlaceableScript != null) {
					thisPlaceableScript.mIsOnFloor = true;
				}
			}

			if (collider.gameObject.name.Contains ("Wall")) {
				if (thisPlaceableScript != null) {
					thisPlaceableScript.mIsOnWall = true;
				}
			}

			if (collider.gameObject.name.Contains ("Conveyor")) {
				if (thisPlaceableScript != null) {
					thisPlaceableScript.mIsOnConveyor = true;
				}
			}
		}
	}
	
	public void OnTriggerExit(Collider collider){

		HideWhenCovered thisHideWhenCovered = gameObject.GetComponent<HideWhenCovered> ();
		HideWhenCovered colliderHideWhenCovered = collider.gameObject.GetComponent<HideWhenCovered> ();

		Placeable thisPlaceableScript = gameObject.GetComponent<Placeable> ();

		if (collider is BoxCollider) {
			if (colliderHideWhenCovered!= null) {
				colliderHideWhenCovered.Reveal (gameObject);

				if (thisHideWhenCovered != null) {
					thisHideWhenCovered.Reveal (gameObject);
				}
			}
			
			if (collider.gameObject.name == "Floor") {
				this.mIsOnFloor = false;
				if (thisPlaceableScript != null) {
					thisPlaceableScript.mIsOnFloor = false;
				}
			}

			if (collider.gameObject.name.Contains ("Wall")) {
				if (thisPlaceableScript != null) {
					thisPlaceableScript.mIsOnWall = false;
				}
			}
			if (collider.gameObject.name.Contains ("Conveyor")) {
				if (thisPlaceableScript != null) {
					thisPlaceableScript.mIsOnConveyor = false;
				}
			}

			if (mCurrentCollider != null) {
				if (mCurrentCollider.GetInstanceID () == collider.GetInstanceID ()) {
					mCurrentCollider = null;
				}
			}
		}
	}

	public void Attached(bool state){
		if(state)		{
			EventHandler.OnPlace += DelayedHide;

		}else		{
			EventHandler.OnPlace -= DelayedHide;
		}
	}

	void OnDestroy(){
		EventHandler.OnPlace -=DelayedHide;
	}

	public void DelayedHide(){

		if (mCurrentCollider != null) {
			HideWhenCovered mHideWhenCoveredScript = mCurrentCollider.gameObject.GetComponent<HideWhenCovered> ();

			if(this != null){
				if (mHideWhenCoveredScript != null && gameObject != null) {	
					mHideWhenCoveredScript.Hide (gameObject);	
				}
			}else{
				Debug.LogWarning("apparently this is null");
			}

		}

		mCurrentCollider = null;
	}
}
