﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ValidPlaceablePosition : MonoBehaviour {

	public int mPlaceableHazardLayerInt = 12;
	[HideInInspector]public bool mIsValidPosition;
	
	private Collider mMainBodyCollidingObject;
	private string[] mIllegalObjectsTagList = {"Placeable", "Foreground", "Furnace", "Window", "Spawner or Collector", "WallInside"}; 
	
	void OnTriggerExit(Collider incomingCollider){

		if (incomingCollider is BoxCollider) {
			if (mMainBodyCollidingObject == incomingCollider) {
				mMainBodyCollidingObject = null;
			}

			IsValidPosition ();
		}
	}
	
	void OnTriggerEnter(Collider incomingCollider){

		if (incomingCollider is BoxCollider) {
			if (mMainBodyCollidingObject != null) {
				if (incomingCollider.gameObject.layer == mPlaceableHazardLayerInt) {
					mMainBodyCollidingObject = incomingCollider;
				}
			} else {
				mMainBodyCollidingObject = incomingCollider;
			}

			IsValidPosition ();
		}
	}
	
	public bool IsValidPosition(){
		
		mIsValidPosition = true;
		IsThisPlaceableASpringOrCannon("Spring");
		IsThisPlaceableASpringOrCannon("Cannon");
		IsThisPlaceableAFan();
		IsThisAnIllegalBackgroundSpace();
		
		return mIsValidPosition;
	}
	
	void IsThisAnIllegalBackgroundSpace(){
		if(mMainBodyCollidingObject != null){
			if(HasAnIllegalTag(mMainBodyCollidingObject.tag)){
				mIsValidPosition = false;
			}
		}
	}
	
	bool HasAnIllegalTag(string incomingTag){
		for(int i = 0; i < mIllegalObjectsTagList.Length;i++){
			if(incomingTag == mIllegalObjectsTagList[i]){
				return true;
			}
		}
		
		return false;
	}

	void IsThisPlaceableASpringOrCannon(string name){
		if(this.gameObject.name.Contains (name)){
			mIsValidPosition = this.gameObject.GetComponent<Placeable>().mIsOnFloor;
		}
	}

	void IsThisPlaceableAFan(){
		if(this.gameObject.name.Contains ("Fan")){
			if(this.gameObject.GetComponent<Placeable>().mIsOnFloor && !this.gameObject.GetComponent<Placeable>().mIsOnConveyor){
				mIsValidPosition = true;
			}else{
				mIsValidPosition = false;
			}

			if(this.gameObject.GetComponent<Placeable>().mIsOnWall){
				mIsValidPosition = true;
			}
		}
	}
}
