﻿using UnityEngine;
using System.Collections;

public class AdditionalSeletionPanel : MonoBehaviour {

	public UISprite mToggleSprite;
	public GameObject mRotateSprite;

	private GameObject mSelectedObject;
	[SerializeField]
	private string mMagnetToggle;
	[SerializeField]
	private string mCannonToggle;

	void Start(){
		gameObject.SetActive(false);
	}

	public void ToggleEffect(){
		
		if(mSelectedObject.name.Contains("Magnet")){
			ToggleMagnet();
		}else if(mSelectedObject.name.Contains("Cannon")){
			FireCannon();
		}
	}

	public void NewSelectedObject(GameObject newObject){

		if(!gameObject.activeSelf){
			gameObject.SetActive(true);
		}

		mSelectedObject = newObject;

		if(newObject.GetComponent<Placeable>() == null){
			mRotateSprite.SetActive(false);
		}else{
			mRotateSprite.SetActive(true);
		}

		if(mSelectedObject.gameObject.name.Contains("Magnet")){
			mToggleSprite.gameObject.SetActive(true);
			mToggleSprite.spriteName = mMagnetToggle;
		}else if(mSelectedObject.gameObject.name.Contains("Cannon")){
			mToggleSprite.gameObject.SetActive(true);
			mToggleSprite.spriteName = mCannonToggle;
		}else{
			mToggleSprite.gameObject.SetActive(false);
		}
	}

	public void DeselectObject(){
		mToggleSprite.gameObject.SetActive(false);
		gameObject.SetActive(false);
	}

	public void RotateSelectedObject(){
		mSelectedObject.GetComponent<Placeable>().RotateCounterClockwise();
	}

	void ToggleMagnet(){
		mSelectedObject.GetComponentInChildren<Magnet>().ToggleDirection();
	}

	void FireCannon(){
		mSelectedObject.GetComponentInChildren<Cannon>().FireRobot();
	}


}
