﻿using UnityEngine;
using System.Collections;

public class Placer : MonoBehaviour 
{
	public LayerMask mLayersToHit;

	private float mSnapSize;
	private Placeable mCurrentObject;
	private Toggle3DButton mSelectedObject;
	private Camera mCamera;
	private GameObject mPlayModeCamera;
	private GameObject mGridObject;

	//To prevent people form quickly dragging things into the game and breaking it
	public float mMouseHoldLimit = 0.2f;

	public bool ObjectAttached { get { return mCurrentObject != null; } }
	public bool ObjectSelected { get { return mSelectedObject != null; } }

	void Start () {
		mSnapSize = UnpackLevel.mLevelScaling;
		mCamera = Camera.main;
		mPlayModeCamera = GameObject.FindGameObjectWithTag("PlaymodeCamera");
		mGridObject = GameObject.FindGameObjectWithTag ("Grid");

	}
	
	void Update (){	
		if(Input.GetMouseButtonDown(0)){			
			Ray screenRay = mCamera.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;

			if(Physics.Raycast(screenRay, out hit, Mathf.Infinity, mLayersToHit)){
				if(hit.collider.tag != "MovingPart"){
					Transform curTransform = hit.transform;
					do{
						AttachPlaceable(curTransform.GetComponent<Placeable>(), false);
						SelectObject(curTransform);
						curTransform = curTransform.parent;

					}
					while(curTransform != null && !ObjectAttached);

				}

				if(hit.collider.tag == "TrashCan"){
					hit.collider.GetComponent<TrashCanDelete>().DeleteObject();
				}
			}
		}

		CheckForMouseRelease();
		CheckForTrashCan();
	}
	
	public void DeleteSelectedPlaceable(){
		if(ObjectAttached){
			ToggleGridObject (false);
			this.GetComponent<PlayerObjectButtons>().DeleteObject(mCurrentObject.name);
			Destroy(mCurrentObject.gameObject);
			mCurrentObject = null;
			EventHandler.CallDeleteTool();
		}
	}

	public void CheckForMouseRelease(){
		if(Input.GetMouseButtonUp(0)){
			ToggleGridObject (false);
			if(ObjectAttached){
				if(mCurrentObject.mReadIsAttachedToMouse){
					if(!CheckForTrashCan()){
						DetachCurrentObject();
					}
					else{
						DeleteSelectedPlaceable();
					}
				} 
				else if(ObjectSelected){
					StopCoroutine(DelayAttach());
					mSelectedObject.Toggle();	
				}
			}
			else {
				if(ObjectSelected){
					StopCoroutine(DelayAttach());
					mSelectedObject.Toggle();
				}
			}

			mSelectedObject = null;
			mCurrentObject = null;
		}
	}
	
	public void AttachPlaceable(Placeable newPlaceable, bool spawnedFromMenu){
		if(newPlaceable == null){
			ToggleGridObject (false);
			return;
		}

		mCurrentObject = newPlaceable;
		
		if(spawnedFromMenu){
			ToggleGridObject (true);
			mCurrentObject.AttachToMouse(mSnapSize);
		} 
		else{
			if(mCurrentObject.GetComponentInChildren<Cannon>() != null){
				if(mCurrentObject.GetComponentInChildren<Cannon>().mStoredRobots > 0){
					mCurrentObject.GetComponentInChildren<PlaceableMovementTransitionMode>().Flash();

				}else{
					StartCoroutine(DelayAttach());

				}
			}else{
				StartCoroutine(DelayAttach());
			}
		}
	}

	IEnumerator DelayAttach(){

		yield return new WaitForSeconds(mMouseHoldLimit);

		ToggleGridObject (true);
		if(mCurrentObject != null){
			mCurrentObject.AttachToMouse(mSnapSize);
		}
	}

	public void SelectObject(Transform transform){
		if(transform == null){
			ToggleGridObject (false);
			return;
		}

		if((transform.GetComponent<Toggle3DButton>() != null)){
			mSelectedObject = transform.GetComponent<Toggle3DButton>();
		}
		else if(transform.GetComponentInChildren<Toggle3DButton>() != null){
			mSelectedObject = transform.GetComponentInChildren<Toggle3DButton>();
		}
		else{
			ToggleGridObject (false);
			mSelectedObject = null;
		}
	}
	
	public void DetachCurrentObject(){
		mSelectedObject = null;

		if(!ObjectAttached)
			return;
		
		Vector3 mouseLocation = mCamera.ScreenToWorldPoint(Input.mousePosition);

		if(gameObject.GetComponent<Budget>().AbleToPlace(mCurrentObject.name)){
			mCurrentObject.Place(mouseLocation);
			EventHandler.CallMoveObject();
		}else{
			DeleteSelectedPlaceable();
		}

		mCurrentObject = null;
	}

	bool CheckForTrashCan(){
		if(this.mPlayModeCamera.camera == null){
			return false;
		}

		Ray screenRay = mPlayModeCamera.camera.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;
		if(Physics.Raycast(screenRay, out hit)){
			return (hit.collider.tag == "TrashCan");
				
		}
		
		return false;
	}

	void ToggleGridObject(bool state){
		mGridObject.GetComponent<FadeInGrid> ().StartFade(state);
	}

}
