﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour {

	//These 2 static values are used for Building the levels in the editor and should not be used elsewhere
	public static Vector2 mLevelSize = new Vector2(20, 15);
	public static float mHeightStartOffset = 5.45f;
	public static float mMouseMoveZone = 12;

	[SerializeField]private float mCameraScrollMultiplier;
	[SerializeField]private float mStiffness;
	[SerializeField]private float mDampening;

	[SerializeField]private CameraBounds mCameraBounds;
	[SerializeField]private Placer mPlacer;
	[SerializeField]private float mScrollSensitivity = 1;

	private bool mReadyToDrag;
	private Vector3 mDesiredPosition;
	private Vector2 mVelocity;
	private bool mIsUnLocked = true;

	private float mMaxZoomableVelocity = 5f;

	void Awake() {
		if (mCameraBounds == null) {
			Debug.LogWarning("mCameraBounds is null on " + gameObject.name);
		}

		if (mPlacer == null) {
			Debug.LogWarning("mPlacer is null on " + gameObject.name);
		}

		this.mVelocity = Vector3.zero;

		Gesture.onDraggingE += OnDragging;
	}

	void Start(){

		transform.position = Camera.main.transform.position;

		//This needs to be in Start so the UnpackLevel script has time to assign mCameraStartingPosition
		this.mDesiredPosition = transform.position;
	}

	void OnDestroy() {
		Gesture.onDraggingE -= OnDragging;
	}

	//I might need to lock out scrolling events while the camera is in motion.  I am getting stuttering and camera jumping around right now due to poor camera code
	void Update () {
		if(mIsUnLocked){
			this.mReadyToDrag = true;
		}
	
		Vector3 myPosition = this.transform.position;
		float zPos = myPosition.z;

		Vector2 twoDPosition = new Vector2(myPosition.x, myPosition.y);
		
		// Calculate spring force
		Vector2 stretch = twoDPosition - new Vector2(this.mDesiredPosition.x, this.mDesiredPosition.y);
		Vector2 force = -this.mStiffness * stretch - this.mDampening * this.mVelocity;

		// Apply acceleration
		this.mVelocity = force * Time.deltaTime / Time.timeScale;

		twoDPosition += this.mVelocity * Time.deltaTime / Time.timeScale;

		Vector3 threeDPosition = new Vector3 (twoDPosition.x, twoDPosition.y, zPos);

		//Check scrollwheel
		if (Input.mouseScrollDelta.magnitude > 0 && mReadyToDrag && mVelocity.sqrMagnitude < mMaxZoomableVelocity) {
			float scrollWheelAmount = Input.mouseScrollDelta.y;
			
			zPos += scrollWheelAmount * mScrollSensitivity;
			threeDPosition = this.mCameraBounds.GetBoundedPosition (new Vector3 (myPosition.x, myPosition.y, zPos));
			mDesiredPosition.z = threeDPosition.z;
		} else {
			mDesiredPosition.z = zPos;
		}

		if (this.mVelocity.sqrMagnitude < 1f) {
			this.mVelocity = Vector3.zero;
			this.transform.position = new Vector3 (this.mDesiredPosition.x, this.mDesiredPosition.y, ((zPos * 0.90f) + (this.mDesiredPosition.z * 0.1f)));
		} else {
			this.transform.position = new Vector3 (threeDPosition.x, threeDPosition.y, (zPos * 0.90f) + (this.mDesiredPosition.z * 0.1f));
		}

		//There is still the werid jump at the start of every level
	}

	void OnDragging(DragInfo dragInfo){
		if(!this.mReadyToDrag || this.mPlacer.ObjectAttached) {
			return;
		}

		Vector3 tempDesiredPosition = this.mCameraBounds.GetBoundedPosition(this.mDesiredPosition - (new Vector3(dragInfo.delta.x, dragInfo.delta.y, mDesiredPosition.z) *  this.mCameraScrollMultiplier));
		this.mDesiredPosition = new Vector3(tempDesiredPosition.x, tempDesiredPosition.y, tempDesiredPosition.z);
	}

	public void ToggleCameraMovment(bool state){
		mReadyToDrag = state;
		mIsUnLocked = state;
	}

	float DetermineGreatervalue(float value1, float value2){

		if(value1 > value2){
			return value1;
		}else{
			return value2;
		}
	}
}
