﻿using UnityEngine;
using System.Collections;

public class CameraBounds: MonoBehaviour {
	
	public int mScreenViewBuffer = 10;
	public Vector2 mScreenManualOffset = new Vector2(10, -20);
	public Vector2 mScreenManualHeightOffset; //x = up, y = down;
	public Transform mTopRightTransform;
	public int mCloseCameraDistance = -15;
	
	private Vector3 mTopRight;

	private Camera mCamera;
	private float mCameraViewAngle;
	private float mMaxCameraZoom;


	void Start(){
		mCamera = Camera.main;
		mCameraViewAngle = mCamera.fieldOfView/2;

		SetCameraBounds();

	}

	void SetCameraBounds(){
		//mBottomLeft = new Vector3( 0, 0, 0);

#if UNITY_EDITOR
		if(mTopRightTransform != null){
			mTopRight = mTopRightTransform.position;
		}else{
			mTopRight = new Vector3(40, 16, 0);
		}
#endif

		DetermineMaxCameraZoom ();
	}

	public Vector3 GetBoundedPosition(Vector3 proposedPosition, bool zMovedCondition = false){

		if(proposedPosition.z == 0  && mCamera != null){
			proposedPosition.z = mCamera.transform.position.z;
		}

		Vector3 cameraPositonAlongLevelCenterline;
		cameraPositonAlongLevelCenterline.x = (mTopRightTransform.position.x) / 2f;
		cameraPositonAlongLevelCenterline.y = mTopRightTransform.position.y / 2f + Mathf.Abs(mCamera.transform.position.z) * Mathf.Tan (mCamera.transform.rotation.x);
		cameraPositonAlongLevelCenterline.z = Mathf.Clamp (proposedPosition.z, mMaxCameraZoom, mCloseCameraDistance);

		//Debug.Log ("CameraCenterPosition " + cameraPositonAlongLevelCenterline);

		cameraPositonAlongLevelCenterline.z = Mathf.Clamp(proposedPosition.z, mMaxCameraZoom, mCloseCameraDistance);

		proposedPosition.x = Mathf.Clamp(proposedPosition.x, this.LeftLimit(cameraPositonAlongLevelCenterline), this.RightLimit(cameraPositonAlongLevelCenterline));
		proposedPosition.y = Mathf.Clamp(proposedPosition.y, this.DownLimit(cameraPositonAlongLevelCenterline), this.UpLimit(cameraPositonAlongLevelCenterline));
		proposedPosition.z = cameraPositonAlongLevelCenterline.z;

		return proposedPosition;
	}
	
	float RightLimit(Vector3 centerLinePosition){

		if (centerLinePosition.z > mCloseCameraDistance) {
			Debug.LogWarning(gameObject.name + " centerLinePosition.z is greater than -15.  This will cause issue when calculating the Limit");
		}

		float frustumWidth = GetFrustumWidth(centerLinePosition.z);
		float levelWidth = mTopRightTransform.position.x + mScreenViewBuffer;

		float xRightLimit = centerLinePosition.x + (levelWidth - frustumWidth)/2f + mScreenManualOffset.x;
		if (xRightLimit < centerLinePosition.x) {
			xRightLimit = centerLinePosition.x;
		}

		//Debug.Log (frustumWidth + ": frustum " + centerLinePosition.x + " , " + levelWidth);
		//Debug.Log (xRightLimit + ": RightLimit");
		return xRightLimit;
	}

	float LeftLimit(Vector3 centerLinePosition){

		if (centerLinePosition.z > mCloseCameraDistance) {
			Debug.LogWarning(gameObject.name + " centerLinePosition.z  is greater than -15.  This will cause issue when calculating the Limit");
		}

		float frustumWidth = GetFrustumWidth (centerLinePosition.z);
		float levelWidth = mTopRightTransform.position.x + mScreenViewBuffer;
		
		float xLeftLimit = centerLinePosition.x - (levelWidth - frustumWidth)/2f + mScreenManualOffset.y;

		//Debug.Log (frustumWidth + ": frustum " + centerLinePosition.x + " , " + levelWidth);
		if (xLeftLimit > centerLinePosition.x) {
			xLeftLimit = centerLinePosition.x;
		}

		//Debug.Log (xLeftLimit + ": LeftLimit");
		return xLeftLimit;
	}

	float DownLimit(Vector3 centerLinePosition){

		if (centerLinePosition.z > mCloseCameraDistance) {
			Debug.LogWarning(gameObject.name + " centerLinePosition.z  is greater than -15.  This will cause issue when calculating the Limit");
		}

		float flatFrustumHeight = GetFrustumHeight (centerLinePosition.z);
		float actualViewableDistance = flatFrustumHeight / Mathf.Cos (mCameraViewAngle * Mathf.Deg2Rad);//accounting for the camera's tilt
		float levelHeight = mTopRightTransform.position.y + mScreenViewBuffer;

		float yDownLimit = centerLinePosition.y - (levelHeight -  actualViewableDistance) + mScreenManualHeightOffset.y;

		if (yDownLimit > centerLinePosition.y) {
			yDownLimit = centerLinePosition.y;
		}

		//Debug.Log (yDownLimit + ": DownLimit");
		return yDownLimit;
	}

	float UpLimit(Vector3 centerLinePosition){

		if (centerLinePosition.z > mCloseCameraDistance) {
			Debug.LogWarning(gameObject.name + " centerLinePosition.z  is greater than -15.  This will cause issue when calculating the Limit");
		}

		float flatFrustumHeight = GetFrustumHeight (centerLinePosition.z);
		float actualViewableDistance = flatFrustumHeight / Mathf.Cos (mCameraViewAngle * Mathf.Deg2Rad);//accounting for the camera's tilt
		float levelHeight = mTopRightTransform.position.y + mScreenViewBuffer;

		float yUpLimit = centerLinePosition.y + (levelHeight -  actualViewableDistance) + mScreenManualHeightOffset.x;	
		if (yUpLimit < centerLinePosition.y) {
			yUpLimit = centerLinePosition.y;
		}


		//Debug.Log (yUpLimit + ": UpLimit");
		return yUpLimit;
	}

	float GetFrustumHeight(float zDistance){
	
		return 2.0f * Mathf.Abs (zDistance) * Mathf.Tan(mCameraViewAngle * Mathf.Deg2Rad);;
	}

	float GetFrustumWidth(float zDistance){

		float height = GetFrustumHeight(zDistance);

		return height * Camera.main.aspect;
	}

	void DetermineMaxCameraZoom(){

		float heightForWidth = mTopRight.x * Camera.main.aspect;
		float maxZoomDistance = mCloseCameraDistance;

		if (heightForWidth < mTopRight.y) {
			maxZoomDistance = (heightForWidth + 2 * mScreenViewBuffer) * 0.5f / Mathf.Tan (mCameraViewAngle * Mathf.Deg2Rad);
		} else {
			maxZoomDistance = (mTopRight.y + 2 * mScreenViewBuffer) * 0.5f / Mathf.Tan (mCameraViewAngle * Mathf.Deg2Rad);
		}

		Debug.Log (-maxZoomDistance + "Max");

		mMaxCameraZoom = -maxZoomDistance;

		//mMaxCameraZoom is negative
		if (mCamera.transform.position.z < mMaxCameraZoom) {
			Vector3 tempVector = mCamera.transform.position;
			tempVector.z = mMaxCameraZoom;
			mCamera.transform.position = tempVector;
		}
	}
	
}
