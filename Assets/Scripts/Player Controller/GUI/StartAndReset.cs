﻿using UnityEngine;
using System.Collections;

public class StartAndReset : MonoBehaviour {

	public static bool mIsGameRunning = false;

	public GameObject[] mStoredPrefabs;
	
	private GameObject[] mAllObjects;
	private Quaternion[] mAllLevelPieceRotations;
	private Vector3[] mAllLevelPiecePositions;
	private string[] mLevelPieceNames;
	
	private GameObject[] mAllMovingParts;
	private string[] mMovingPartNames;
	private InBuildDebugging mDebuggingScript;

	public void StartGame(){

		if(!mIsGameRunning){
			mIsGameRunning = true;

		}
	}
		
	public void StopGame(){
		if(mIsGameRunning){
			DeleteRobots();
			mIsGameRunning = false;
		}
	}

	void DeleteRobots(){
		GameObject[] allRobots = GameObject.FindGameObjectsWithTag("Robot");

		for(int i = 0; i < allRobots.Length; i++){
			Destroy(allRobots[i]);
		}	
	}

	
	void TogglePlacer(bool state){
		GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Placer>().enabled = state;
		
	}

	public bool IsGameRunning(){
		return mIsGameRunning;
	}

}
