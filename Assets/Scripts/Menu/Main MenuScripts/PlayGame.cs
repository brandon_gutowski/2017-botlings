﻿using UnityEngine;
using System.Collections;

public class PlayGame : MonoBehaviour {

	public string mTutorial;
	public string mLevelSelectName;

	void Awake(){
		//SaveandLoad.Load and .Save are run to ensure everything is runnign smoothly upon game start
		SaveAndLoad.Load();
		SaveAndLoad.Save();
	}

	public void Play(){
		if(PlayerData.instance.mLastTutorialSeen < 5){
			Application.LoadLevel(mTutorial);
		}else{
			Application.LoadLevel(mLevelSelectName);
		}
	}
}
