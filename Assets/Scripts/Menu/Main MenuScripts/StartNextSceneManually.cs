﻿using UnityEngine;
using System.Collections;

public class StartNextSceneManually : MonoBehaviour {

	//Only use this script as a way to force a level change incase the gaem freezes or buttons become glitched.  
	//In most cases this script should not be used.

	public float mTimeToWait = 10;

	private float mTimeSinceCreation;

	void Start () {
		mTimeSinceCreation = 0.0f;

	}

	void Update () {

		mTimeSinceCreation += Time.deltaTime;

		if(mTimeSinceCreation > mTimeToWait){

			Application.LoadLevel(Application.loadedLevel + 1);
		}

	}
}
