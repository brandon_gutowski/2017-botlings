﻿using UnityEngine;
using System.Collections;

public class OpenExternalSite : MonoBehaviour {

	public string mWebsiteToOpen;//format www.websitename.com

	void OnClick(){

		Application.OpenURL(mWebsiteToOpen);

	}
}
