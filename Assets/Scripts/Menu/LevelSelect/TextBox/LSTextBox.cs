using UnityEngine;
using System.Collections;

public class LSTextBox : MonoBehaviour {

	public UILabel mTitleLabel;
	public UILabel mBestTimeLabel;
	public UILabel mMovesLabel;
	public UILabel mRobotsUsedLabel;
	public UILabel mToolsRemainingLabel;
	public GameObject mStartButton;
	public GameObject mBestTimeTitle;

	void Awake(){
		SetDefaultValues ();
		LevelSelectEventHandler.OnLevelIconSelected += NewLevelSelected;
	}

	void OnDestroy(){
		LevelSelectEventHandler.OnLevelIconSelected -= NewLevelSelected;
	}

	private void NewLevelSelected(GameObject selectedLevel){

		int levelInt = selectedLevel.GetComponent<MenuLevelItem>().mLevelLoadInt;

		LevelData currentLevelData = PlayerData.instance.mLevelDataList [levelInt];

		if (currentLevelData.mIsComplete) {
			UpdateLabels (currentLevelData);
		} else {
			LevelNotComplete(currentLevelData);
		}

	}

	void SetDefaultValues(){
		mTitleLabel.text = "";
		mBestTimeLabel.text = "  Please select";
		mMovesLabel.text = " a level";
		mRobotsUsedLabel.text = "";
		mToolsRemainingLabel.text = "";
		mStartButton.SetActive (false);
		mBestTimeTitle.SetActive (false);

	}

	void UpdateLabels(LevelData currentData){

		UpdateTitleLabel (currentData.mID);
		UpdateBestTimeLabel (currentData.mTimeToComplete);
		UpdateMovesLabel (currentData.mMoves);
		UpdateRobotsLabel (currentData.mRobotsUsed);
		UpdateToolsLabel (currentData.mToolsUsed);

		if (!mStartButton.activeSelf) {
			mStartButton.SetActive (true);
		}
		if (!mBestTimeTitle.activeSelf) {
			mBestTimeTitle.SetActive(true);
		}
	}

	void LevelNotComplete(LevelData currentData){

		UpdateTitleLabel (currentData.mID);
		mBestTimeLabel.text = "NA";
		mMovesLabel.text = "Moves   NA";
		mRobotsUsedLabel.text = "Robots NA";
		mToolsRemainingLabel.text = "Tools   NA";
	
		if (!mStartButton.activeSelf) {
			mStartButton.SetActive (true);
		}
		if (!mBestTimeTitle.activeSelf) {
			mBestTimeTitle.SetActive(true);
		}

	}

	void UpdateTitleLabel(int level){
		mTitleLabel.text = "Level " + (1 + (level -1)/10).ToString()+ "-" + (1 + (level - 1)%10);
	}

	void UpdateBestTimeLabel(float time){
		mBestTimeLabel.text = string.Format ("{0:0}:{1:00}", Mathf.Floor(time/60),time%60);
	}

	void UpdateMovesLabel(int moves){
		mMovesLabel.text = "Moves   " + moves;
	}

	void UpdateRobotsLabel(int robots){
		mRobotsUsedLabel.text = "Robots  " + robots;
	}

	void UpdateToolsLabel(int tools){
		mToolsRemainingLabel.text = "Tools   " + tools;
	}

}
