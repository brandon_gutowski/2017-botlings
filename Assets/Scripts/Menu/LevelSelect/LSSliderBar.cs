﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LSSliderBar : MonoBehaviour {
	
	/*public GameObject mAssociatedLevelGroupReveal;
	public GameObject mAssociatedLevelGroupHide;
	public List<GameObject> mPanelStorageGrids = new List<GameObject>();
	public GameObject[] mDropContainers;
	public float mCurrentAnchorPosition;

	public static float mPanelLength = 500;

	private LSLevelGroup mLevelGroupReveal;
	private LSLevelGroup mLevelGroupHide;
	public static float mLeftGridPosition = 0f;
	public static float mRightGridPosition = 0f;
	private float mCurrentRelativeX;

	public void Start(){

		mLevelGroupReveal = mAssociatedLevelGroupReveal.GetComponent<LSLevelGroup>();
		mLevelGroupHide = mAssociatedLevelGroupHide.GetComponent<LSLevelGroup>();

		for(int i = 0; i < mDropContainers.Length; i++){
			mDropContainers[i].SetActive(false);
		}
	}

	public void OnDragEnd(){

		if(transform.parent.name == "Grid Right") UpdateAnchorPosition(1.0f);
		if(transform.parent.name == "Grid Left") UpdateAnchorPosition(0.0f);

		for(int i = 0; i < mDropContainers.Length; i++){
			mDropContainers[i].SetActive(false);
		}

		EventHandler.CallSliderDragEnd();
	}

	public void OnDrag(){
		UpdateAnchorPosition();

		for(int i = 0; i < mDropContainers.Length; i++){
			mDropContainers[i].SetActive(true);
		}
	}

	void UpdateAnchorPosition(){

		float percentOfPosition;

		mCurrentRelativeX = transform.localPosition.x + Mathf.Abs((int)mLeftGridPosition);
		percentOfPosition = mCurrentRelativeX/(Mathf.Abs((int)mLeftGridPosition) + Mathf.Abs((int)mRightGridPosition));

		mLevelGroupReveal.SetRevealPercentage(percentOfPosition);

		if(mLevelGroupHide !=null){
			mLevelGroupHide.SetHidePercentage(1 - percentOfPosition);
		}
				
	}

	void UpdateAnchorPosition(float percentOfPosition){
		if(percentOfPosition < 0.0f || percentOfPosition > 1.0f){
			Debug.LogError(percentOfPosition);
		}
		
		mLevelGroupReveal.SetRevealPercentage(percentOfPosition);

		if(mLevelGroupHide !=null){
			mLevelGroupHide.SetHidePercentage(1 - percentOfPosition);
		}
	}*/

}
