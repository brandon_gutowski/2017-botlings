﻿using UnityEngine;
using System.Collections;

public class LSRobotLabel : MonoBehaviour {

	// Use this for initialization
	void Awake () {

		UpdateLabel ();
		InvokeRepeating ("UpdateLabel", 2f, 2f);

		LevelSelectEventHandler.OnDailyCheckCalled += DailyRobotCheckUpdateLabel;
	}

	void OnDestroy(){
		LevelSelectEventHandler.OnDailyCheckCalled -= DailyRobotCheckUpdateLabel;

	}

	private void DailyRobotCheckUpdateLabel(int robot){
		UpdateLabel ();
	}

	private void UpdateLabel(){
		if (PlayerData.instance.mUnlimitedRobotsUnlocked) {
			PlayerData.instance.mNumberOfRobots = 999;
		}
		
		gameObject.GetComponent<UILabel> ().text = "x " + PlayerData.instance.mNumberOfRobots;
	}
	

}
