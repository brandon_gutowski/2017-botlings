﻿using UnityEngine;
using System.Collections;

public class NextLevel : MonoBehaviour {

	//The Design choice for the next level button has the button bring the player to the next level, but every 10 levels, the player returns to the level select screen
	public void StartNextLevel(){

		if(StartAndReset.mIsGameRunning){
			StartAndReset.mIsGameRunning = !StartAndReset.mIsGameRunning;
		}

		if (Currentlevel.instance.mID % 10 != 0 || Currentlevel.instance.mID == 0) {

			EventHandler.CallNewLevelLoaded (Currentlevel.instance.mID + 1);
	
			if (Currentlevel.instance.mID == 0) {
				Currentlevel.instance.NewLevel (1);
			}
			Application.LoadLevel ((Currentlevel.instance.mID).ToString () + " SpringItLevel");
		} else {

			Application.LoadLevel("Level Select Screen");
		}

	}
}
