﻿using UnityEngine;
using System.Collections;

public class OpenDebugPanel : MonoBehaviour {
	
	public GameObject mDebugPanel;

	void Update(){
		if(Input.GetKeyDown(KeyCode.D)){
			mDebugPanel.SetActive(!mDebugPanel.activeSelf);
		}

		if(mDebugPanel.activeSelf){
			mDebugPanel.GetComponent<InBuildDebugging>().UpdateLevelList();
		}
	}
}
