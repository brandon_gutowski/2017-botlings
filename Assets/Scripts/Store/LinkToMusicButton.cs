﻿using UnityEngine;
using System.Collections;

public class LinkToMusicButton : MonoBehaviour {

	public string mStoreListingToOpen = "market://details?id=album-Boqnka5pdmqsxi5tjsit4fqduhu";

#if UNITY_ANDROID
	//Opens up Dash's and Alistar's Music
	public void OpenInGooglePlay(){

		Application.OpenURL(mStoreListingToOpen);
	}

#endif



}
